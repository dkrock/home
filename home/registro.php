<?php


?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Registro</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <style>
  	body{
  		background: grey;
  	}
  </style>


  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
	<div class="registro">
		<form class="control-group" method="post" action="pages/insert_user.php">
			<table class="t_registtro table" border=0>	
				<tr>
					<td rowspan="10">
						<img src="imag/registro.png">
					</td>
					<td colspan="2"><h3>Nuevo Usuario</h3></td>
				</tr>		
				<tr>
					<td>Nombres</td>
					<td><input type="text" class="form-control" name="nombres"></td>
				</tr>
				<tr>
					<td>Apellidos</td>
					<td><input type="text" class="form-control" name="apellidos"></td>
				</tr>
				
				<tr>
					<td>Edad</td>
					<td><input type="number" class="form-control" name="edad"></td>
				</tr>
				<tr>
					<td>Genero</td>
					<td>
						<select name="genero" class="form-control">
							<option value="M">Masculino</option>
							<option value="F">Femenino</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Mail</td>
					<td><input type="email" class="form-control" name="mail"></td>
				</tr>
				<tr>
					<td>Pais</td>
					<td><input type="text" class="form-control" name="pais"></td>
				</tr>
				<tr>
					<td>Cuidad</td>
					<td><input type="text" class="form-control" name="ciudad"></td>
				</tr>
				<tr>
					<td>Usuario</td>
					<td><input type="text" class="form-control" name="usuario"></td>
				</tr>
				<tr>
					<td>password</td>
					<td><input type="password" class="form-control" name="password"></td>
				</tr>
				<tr>
					<td colspan="2">							
						<button class="btn btn-default" name="cancelar">Cancelar</button>
						<input type="submit" class="btn btn-default" value="Registrar">	
					</td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
  <script src="js/scripts.js"></script>
</body>
</html>