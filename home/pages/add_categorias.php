<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>HOME</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="../css/style.css">


  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
	<div class="container">
		<div class="jumbotron">
		<div class="row">			
			<div class="col-xs-12"><span class="text"><h2 style="text-align:right">Categorias</h2></span></div>
		</div>		
		<div class="row">
			<div class="col-xs-12">
				<ul>
					<li class="list-inline"><a href="list_categorias.php">Preview</a></li>
				</ul>
				<form action="insert_categoria.php" method="post">
				<table class="table">
					<tr>
						<td>Titulo</td>
						<td><input type="text" class="form-control" name="titulo_categoria"></td>
					</tr>
					<tr>
						<td>Color</td>
						<td><input type="color" class="form-control" name="color_categoria"></td>
					</tr>					
					<tr>
						<td colspan=2>
							<select name="estatus" class="form-control">
								<option value="1">Activo</option>
								<option value="0">Inactivo</option>
							</select>
						</td>
					</tr>
					<tr>						
						<td colspan=2><input type="submit" class="form-control" name="enviar"></td>
					</tr>
				</table>
				</form>
			</div>
		</div>
	</div>
	</div>

  <script src="js/scripts.js"></script>
</body>
</html>