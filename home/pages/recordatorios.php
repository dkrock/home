<?php
session_start();
include"../conexion.php";

if(isset($_SESSION['misession']) && $_SESSION['misession']==1)
{

	$id_usuario = $_SESSION['id_usuario'];
	$sql = "select * from categorias where id_usuario=$id_usuario && estatus=1";
	$statemement = mysql_query($sql)or die(mysql_error()."Error en la peticion");

?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>HOME</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="../css/style.css">


  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
	<div class="container">
		<div class="jumbotron">
		<div class="row">
			<div class="col-xs-12 titulo">
				<h2>Recordatorio</h2>
			</div>
			<div class="col-xs-12">
				<form action="insert_recordatorio.php" method="post" name="recordatorios">
				<table class='table' border=0>
					<tr>
						<td><span class="titulos">Estado</span></td>
						<td>
							<select name="estatus" class="form-control">
								<option value="1">Activo</option>
								<option value="2">Inactivo</option>
							</select>
						</td>						
					</tr>
					<tr>																				
						<td><span class="titulos">Categoria</span></td>
						<td>
						<select name="categoria" class="form-control">
								<?php
								while($row = mysql_fetch_array($statemement))
								{
									?>
									<option value="<?php echo $row['id_categoria']; ?>"><?php echo $row['titulo_categoria']; ?></option>
									<?php
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td><span class="titulos">Titulo</span></td>
						<td>
							<input type="text" class="form-control" name="titulo">
						</td>
					</tr>

					<tr>					
						<td><span class="titulos">Creado Para</span></td>
						<td><input type="date" class="form-control" name="fecha"></td>
					</tr>
					<tr>
						<td><span class="titulos">Comentario</span></td>
						<td>
							<textarea class="form-control" name="comentario">
							</textarea>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="form-control" name="enviar"></td>
					</tr>
				</table>
				</form>
			</div>
			<div class="col-xs-12"></div>
		</div>		
	</div>
	</div>

  <script src="js/scripts.js"></script>
</body>
</html>

<?php

}
else
{
	echo "Registrate e inicia session";
	?>
		<script type="text/javascript">
        	function redireccionar()
            	{
                    //alert("DATOS INSERTADOS");
                	window.location="../index.php" ;
                }
                setTimeout ("redireccionar()", 1000); 
        </script> 
	<?php
}
?>