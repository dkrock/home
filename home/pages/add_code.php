<?php
session_start();
include"../conexion.php";

if(isset($_SESSION['misession']) && $_SESSION['misession']==1)
{
	$id_usuario = $_SESSION['id_usuario'];


?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>HOME</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="../css/style.css">


  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="jumbotron">
					<table class="table">
						<tr>
							<td>Lenguaje</td>
							<td><input type="text" class="form-control" name="leguaje"></td>		
						</tr>
						<tr>
							<td>Titulo</td>
							<td><input type="text" class="form-control" name="titulo"></td>
						</tr>
						<tr>
							<td>Descripcion</td>
							<td><input type="text" class="form-control" name="descripcion"></td>
						</tr>
						<tr>
							<td>Code</td>
							<td><textarea class="form-control" name="code" value=""></textarea></td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="submit" class="form-control" name="enviar" value="Insertar">
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

 <script src="js/scripts.js"></script>
</body>
</html>

<?php

}
else
{
	echo "Registrate e inicia session";
	?>
		<script type="text/javascript">
        	function redireccionar()
            	{
                    //alert("DATOS INSERTADOS");
                	window.location="../index.php" ;
                }
                setTimeout ("redireccionar()", 1000); 
        </script> 
	<?php
}
?>