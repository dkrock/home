-- --------------------------------------------------------
-- Host:                         
-- Server version:               5.5.8 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-07-25 13:15:39
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for tlife_menu
DROP DATABASE IF EXISTS `tlife_menu`;
CREATE DATABASE IF NOT EXISTS `tlife_menu` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `tlife_menu`;


-- Dumping structure for table tlife_menu.tlife_menu_pages
DROP TABLE IF EXISTS `tlife_menu_pages`;
CREATE TABLE IF NOT EXISTS `tlife_menu_pages` (
  `page_id` int(100) NOT NULL AUTO_INCREMENT,
  `page_nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `page_url` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `page_imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `page_categoria` int(10) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table tlife_menu.tlife_menu_pages: ~22 rows (approximately)
DELETE FROM `tlife_menu_pages`;
/*!40000 ALTER TABLE `tlife_menu_pages` DISABLE KEYS */;
INSERT INTO `tlife_menu_pages` (`page_id`, `page_nombre`, `page_url`, `page_imagen`, `page_categoria`) VALUES
	(1, 'TheCore', 'http://tagsv.ti.ads/thecore', 'thecore.png', 0),
	(2, 'ProjectManager', 'https://secure.projectmanager.com/Login.aspx?ReturnUrl=%2f', 'pm.jpg', 0),
	(3, 'Doc - Bugs', 'https://docs.google.com/a/telusinternational.com/spreadsheet/ccc?key=0AjAnWcBrrgYkdDJ4QkVVcjY2dk4tOVVsYVdYa2JQT0E&usp=sharing_eil#gid=9', 'docs.png', 0),
	(4, 'Information S.', 'https://sites.google.com/a/telusinternational.com/is/', 'is.png', 0),
	(5, 'T.Life Local', 'http://localhost/tlifesv/', 'tlife.png', 0),
	(6, 'Summary', 'http://localhost/tlifesv/page/summary', 'summary.png', 0),
	(7, 'Calendar', 'https://www.google.com/calendar/render?tab=mc&pli=1', 'calendar.jpg', 0),
	(8, 'T.Life SV', 'http://tlifesv.ti.ads/book/login_form.php?url=%2Fbook%2Findex.php', 'tlife.png', 0),
	(9, 'SysAid', 'http://ti.sysaidit.com/Login.jsp', 'sysaid.png', 0),
	(10, 'T.Life DEV', 'http://172.23.36.41:8001/book/login_form.php?url=%2Fbook%2F', 'tlife.png', 0),
	(11, 'Webex', 'http://www.webex.com/index-sky-2.html', 'CIsco-webex-logo.png', 0),
	(12, 'BOOK', 'mybook.php', 'misdocs.gif', 0),
	(13, 'TeamHub', 'https://telus.plateau.com/learning/user/nativelogin.do', 'teamhoub.png', 0),
	(14, 'TAG', 'http://tagsv.ti.ads/boleta_referidos/', 'tag.png', 0),
	(15, 'Facebook', 'https://www.facebook.com/', 'Facebook_like_buton.png', 1),
	(16, 'Twitter', 'https://twitter.com/login', 'Twitter_logo_blue.png', 1),
	(17, 'EDH', 'https://epaper.elsalvador.com/', 'eldiariodehoy.png', 1),
	(18, 'OLX', 'http://www.olx.com.sv/', 'logo_olx.png', 1),
	(19, 'Bootstrap', 'http://getbootstrap.com/', 'bootstrap-logo.png', 1),
	(20, 'Google', 'https://www.google.com.sv/', 'social_google_box.png', 1),
	(21, 'Hotmail', 'https://www.hotmail.com/', 'Live Hotmail.png', 1),
	(22, 'Gmail', 'https://mail.google.com/', 'Gmail_Icon.png', 1);
/*!40000 ALTER TABLE `tlife_menu_pages` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
