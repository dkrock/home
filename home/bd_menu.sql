-- --------------------------------------------------------
-- Host:                         
-- Server version:               5.5.8 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-07-29 16:34:27
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for tlife_menu
DROP DATABASE IF EXISTS `tlife_menu`;
CREATE DATABASE IF NOT EXISTS `tlife_menu` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `tlife_menu`;


-- Dumping structure for table tlife_menu.login
DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id_login` int(100) NOT NULL AUTO_INCREMENT,
  `nombre_login` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_login` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `password_login` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table tlife_menu.login: ~1 rows (approximately)
DELETE FROM `login`;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`id_login`, `nombre_login`, `usuario_login`, `password_login`) VALUES
	(1, 'Rafael Fernando Gutierrez Tejada', 'rafael', 'rafael');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


-- Dumping structure for table tlife_menu.recordatorios
DROP TABLE IF EXISTS `recordatorios`;
CREATE TABLE IF NOT EXISTS `recordatorios` (
  `recordatorio_id` int(100) NOT NULL AUTO_INCREMENT,
  `recordatorio_titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `recordatorio_fecha` date NOT NULL,
  `recordatorio_comentario` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `recordatorio_color` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `recordatorio_idusuario` int(100) NOT NULL,
  PRIMARY KEY (`recordatorio_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table tlife_menu.recordatorios: ~1 rows (approximately)
DELETE FROM `recordatorios`;
/*!40000 ALTER TABLE `recordatorios` DISABLE KEYS */;
INSERT INTO `recordatorios` (`recordatorio_id`, `recordatorio_titulo`, `recordatorio_fecha`, `recordatorio_comentario`, `recordatorio_color`, `recordatorio_idusuario`) VALUES
	(1, 'Cena', '2014-07-29', 'cena hoy, solo con un trago......							', '#ce0005', 2);
/*!40000 ALTER TABLE `recordatorios` ENABLE KEYS */;


-- Dumping structure for table tlife_menu.tlife_menu_pages
DROP TABLE IF EXISTS `tlife_menu_pages`;
CREATE TABLE IF NOT EXISTS `tlife_menu_pages` (
  `page_id` int(100) NOT NULL AUTO_INCREMENT,
  `page_nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `page_url` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `page_imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `page_categoria` int(10) NOT NULL,
  `id_usuario` int(100) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table tlife_menu.tlife_menu_pages: ~29 rows (approximately)
DELETE FROM `tlife_menu_pages`;
/*!40000 ALTER TABLE `tlife_menu_pages` DISABLE KEYS */;
INSERT INTO `tlife_menu_pages` (`page_id`, `page_nombre`, `page_url`, `page_imagen`, `page_categoria`, `id_usuario`) VALUES
	(1, 'TheCore', 'http://tagsv.ti.ads/thecore', 'thecore.png', 0, 2),
	(2, 'ProjectManager', 'https://secure.projectmanager.com/Login.aspx?ReturnUrl=%2f', 'pm.jpg', 0, 2),
	(3, 'Doc - Bugs', 'https://docs.google.com/a/telusinternational.com/spreadsheet/ccc?key=0AjAnWcBrrgYkdDJ4QkVVcjY2dk4tOVVsYVdYa2JQT0E&usp=sharing_eil#gid=9', 'docs.png', 0, 2),
	(4, 'Information S.', 'https://sites.google.com/a/telusinternational.com/is/', 'is.png', 0, 2),
	(5, 'T.Life Local', 'http://localhost/tlifesv/', 'tlife.png', 0, 2),
	(6, 'Summary', 'http://localhost/tlifesv/page/summary', 'summary.png', 0, 2),
	(7, 'Calendar', 'https://www.google.com/calendar/render?tab=mc&pli=1', 'calendar.jpg', 0, 2),
	(8, 'T.Life SV', 'http://tlifesv.ti.ads/book/login_form.php?url=%2Fbook%2Findex.php', 'tlife.png', 0, 2),
	(9, 'SysAid', 'http://ti.sysaidit.com/Login.jsp', 'sysaid.png', 0, 2),
	(10, 'T.Life DEV', 'http://172.23.36.41:8001/book/login_form.php?url=%2Fbook%2F', 'tlife.png', 0, 2),
	(11, 'Webex', 'http://www.webex.com/index-sky-2.html', 'CIsco-webex-logo.png', 0, 2),
	(12, 'BOOK', 'mybook.php', 'misdocs.gif', 0, 2),
	(13, 'TeamHub', 'https://telus.plateau.com/learning/user/nativelogin.do', 'teamhoub.png', 0, 2),
	(14, 'TAG', 'http://tagsv.ti.ads/boleta_referidos/', 'tag.png', 0, 2),
	(15, 'Facebook', 'https://www.facebook.com/', 'Facebook_like_buton.png', 1, 2),
	(16, 'Twitter', 'https://twitter.com/login', 'Twitter_logo_blue.png', 1, 2),
	(17, 'EDH', 'https://epaper.elsalvador.com/', 'eldiariodehoy.png', 1, 2),
	(18, 'OLX', 'http://www.olx.com.sv/', 'logo_olx.png', 1, 2),
	(19, 'Bootstrap', 'http://getbootstrap.com/', 'bootstrap-logo.png', 1, 2),
	(20, 'Google', 'https://www.google.com.sv/', 'social_google_box.png', 1, 2),
	(21, 'Hotmail', 'https://www.hotmail.com/', 'Live Hotmail.png', 1, 2),
	(22, 'Gmail', 'https://mail.google.com/', 'Gmail_Icon.png', 1, 2),
	(23, 'TICKET', 'http://tagsv.ti.ads/workflows', 'ticket.png', 1, 2),
	(24, 'V2B', 'https://www.video2brain.com/mx/', '1405268154_unnamed.png', 1, 2),
	(25, 'Mejorandola', 'https://mejorando.la/', 'mejorandola.jpg', 1, 2),
	(26, 'Google', 'https://www.google.com.sv/', 'Google_chrome_logo.png', 1, 4),
	(27, 'INFINITE', 'http://www.infiniteskills.com/', 'gI_89926_CD-Logo.png', 1, 4),
	(28, 'Outlook', 'https://outlook.com/', 'outlooklogouse.png', 1, 3),
	(29, 'Tlife Site', 'https://docs.google.com/a/telusinternational.com/spreadsheet/ccc?key=0Anrt8FR4zJgWdDlxZ0xsTnduakFpRE9sQTlmWjVEUEE&usp=sharing_eil#gid=0', 'excel.png', 1, 2);
/*!40000 ALTER TABLE `tlife_menu_pages` ENABLE KEYS */;


-- Dumping structure for table tlife_menu.usuarios
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(100) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(10) NOT NULL,
  `genero` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` int(10) NOT NULL,
  `pais` int(10) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Dumping data for table tlife_menu.usuarios: ~3 rows (approximately)
DELETE FROM `usuarios`;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id_usuario`, `nombres`, `apellidos`, `edad`, `genero`, `mail`, `ciudad`, `pais`, `usuario`, `password`) VALUES
	(2, 'Rafael Fernando', 'Gutierrez Tejada', 24, 'M', 'rafa_fer_tejada@hotmil.com', 1, 1, 'rafael', 'rafael'),
	(3, 'Diego Javier', 'Mena leiva', 33, 'M', 'diego.mena@hotmail.com', 1, 1, 'diego', 'diego'),
	(4, 'Laura Nohemi', 'Cardoza Ramoz', 21, 'F', 'laura.cardoza@hotmail.com', 1, 1, 'laura', 'laura');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
